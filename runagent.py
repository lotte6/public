from agent import Sniffer
from time import sleep
Sniffer().start()
try:
    while True:
        sleep(100)
except KeyboardInterrupt:
    print("[*] Stop sniffing")
    Sniffer().join(2.0)

    if Sniffer().isAlive():
        Sniffer().socket.close()